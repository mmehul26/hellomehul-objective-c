//
//  AppDelegate.h
//  HelloMehul
//
//  Created by Mehul Makwana on 30/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

